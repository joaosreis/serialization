using System;

namespace serialization
{
    [Serializable]

    class Professor
    {
        private int id;
        private string nome;

        private string departamento;

        public Professor(int id, string nome, string departamento)
        {
            this.id = id;
            this.nome = nome;
            this.departamento = departamento;
        }

        public override string ToString() { return id + ": " + nome + " - " + departamento; }
    }
}
