﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

namespace serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Estado e = new Estado();

            Aluno a = new Aluno(0, "João");
            Aluno b = new Aluno(1, "Miguel");

            e.adicionarAluno(a);
            e.adicionarAluno(b);

            Professor p = new Professor(10, "Rita", "Informática");

            e.adicionarProfessor(p);

            // Para guardar em ficheiro

            // Criar um formatador
            IFormatter formatador = new BinaryFormatter();
            // Criar uma stream para escrita num ficheiro
            Stream stream = new FileStream("aluno.txt", FileMode.Create, FileAccess.Write);

            // O método serialize do formatador escreve na stream qualquer objeto serializável
            formatador.Serialize(stream, e);
            // Fechamos a stream do ficheiro
            stream.Close();

            // -------------------------------

            // Para ler de um ficheiro

            // Criar um formatador (vamos usar o que foi definido anteriormente)

            // Criar uma stream para leitura de um ficheiro
            stream = new FileStream("aluno.txt", FileMode.Open, FileAccess.Read);
            // O método deserialize do formatador lê da stream qualquer objeto serializável
            Estado e_lido = (Estado)formatador.Deserialize(stream);
            // Fechamos a stream
            stream.Close();

            e.GetAlunos().ForEach(a => Console.WriteLine(a));
            e.GetProfessores().ForEach(p => Console.WriteLine(p));
        }
    }
}
