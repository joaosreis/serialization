using System;
using System.Collections.Generic;

namespace serialization
{
    [Serializable]
    class Estado
    {
        private List<Aluno> alunos;
        private List<Professor> professores;

        public Estado()
        {
            alunos = new List<Aluno>();
            professores = new List<Professor>();
        }

        public void adicionarAluno(Aluno a)
        {
            alunos.Add(a);
        }

        public void adicionarProfessor(Professor p)
        {
            professores.Add(p);
        }

        public List<Aluno> GetAlunos()
        {
            return alunos;
        }

        public List<Professor> GetProfessores()
        {
            return professores;
        }
    }
}
