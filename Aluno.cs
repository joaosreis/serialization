using System;

namespace serialization
{
    [Serializable]

    class Aluno
    {
        private int id;
        private string nome;

        public Aluno(int id, string nome)
        {
            this.id = id;
            this.nome = nome;
        }

        public int GetId() { return id; }

        public string GetNome() { return nome; }


        public override string ToString() { return id + ": " + nome; }
    }
}
